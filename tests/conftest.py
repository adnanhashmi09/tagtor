"""
Tests config
"""
import os
import tempfile

import pytest

from tagtor import create_app
from tagtor.db import get_db_connection


with open(os.path.join(os.path.dirname(__file__), 'data.sql'), 'rb') as f:
    _data_sql = f.read().decode('utf8')

with open(os.path.join(os.path.dirname(__file__), 'schema.sql'), 'rb') as f:
    _schema_sql = f.read().decode('utf8')

@pytest.fixture
def app():

    app = create_app({
        'TESTING': True,
    })

    with app.app_context():
        conn = get_db_connection()
        db = conn.cursor()
        db.execute(_schema_sql)
        db.execute(_data_sql)
        db.close()
        conn.close()

    yield app

@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()
