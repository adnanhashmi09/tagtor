/**
 * SQL test data
 */


INSERT INTO server_status (is_bridge, published, nickname, fingerprint,
  or_addresses, last_seen, first_seen, running, flags, country,
  country_name, autonomous_system, as_name, verified_host_names,
  last_restarted, exit_policy, contacts, platform, version,
  version_status, effective_family, transport, bridgedb_distributor, blocklist
) VALUES(
 't', '2022-08-30 20:54:49', 'wolfTest359', '00B5DE0522A6F215BF694DE36576AF1A3927D6D8',
 '["[fd9f:2e19:3bcf::b3:5224]:61528"]', '2022-08-31 11:54:43', '2022-08-31 11:54:43',
 't', '', '', '', '', '', null, '2022-08-25 23:02:27', '["reject *:*"]',
 'zantimisfit@protonmail.me', 'Tor 0.4.7.8 on OpenBSD', 'tor 0.4.7.8',
 'unrecommended', '','', 'email', ''
);
