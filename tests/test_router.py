"""
App tests
"""
import pytest
from tagtor.db import get_db_connection


def test_index(client):
    response = client.get('/routers')
    assert b"00B5DE0522A6F215BF694DE36576AF1A3927D6D8" in response.data

def test_profile(client):
    response = client.get('/routers/22F74E176F803499D4F80D9CE7D325883A8C0E45')
    assert b"22F74E176F803499D4F80D9CE7D325883A8C0E45" in response.data
