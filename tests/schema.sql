/**
 * SQL schema
 */
 CREATE TABLE IF NOT EXISTS server_status(
   is_bridge                           BOOLEAN                      NOT NULL,
   published                           TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
   nickname                            TEXT                         NOT NULL,
   fingerprint                         TEXT                         NOT NULL,
   or_addresses                        TEXT,
   last_seen                           TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
   first_seen                          TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
   running                             BOOLEAN,
   flags                               TEXT,
   country                             TEXT,
   country_name                        TEXT,
   autonomous_system                   TEXT,
   as_name                             TEXT,
   verified_host_names                 TEXT,
   last_restarted                      TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
   exit_policy                         TEXT,
   contacts                            TEXT,
   platform                            TEXT,
   version                             TEXT,
   version_status                      TEXT,
   effective_family                    TEXT,
   transport                           TEXT,
   bridgedb_distributor                TEXT,
   blocklist                           TEXT,
   PRIMARY KEY(fingerprint, nickname, published)
 );
