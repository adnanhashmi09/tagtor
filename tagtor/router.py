"""
App logic
"""

from flask import (
    Blueprint, render_template, request
)

from tagtor.db import get_db_connection
from tagtor.vm import get_time_series

bp = Blueprint('router', __name__)

TIME_FORMAT = '%Y-%m-%d'

@bp.route('/routers')
def index(page=None):
    conn = get_db_connection()
    db = conn.cursor()

    limit = '10'
    offset = '0'
    page = request.args.get('page')
    if page:
        if int(page) > 1:
            offset = str(10 * (int(page)-1))

    db.execute(
        'SELECT * FROM server_status LIMIT '
        + limit
        + ' OFFSET '
        + offset + ';'
    )
    routers = db.fetchall()
    db.close()
    conn.close()

    context = {
        "title": "Tor nodes",
        "routers": routers,
        "page": page,
    }
    return render_template('router/index.html', **context)

@bp.route('/routers/<fingerprint>')
def profile(fingerprint):
    conn = get_db_connection()
    db = conn.cursor()
    db.execute(
        'SELECT * FROM server_status WHERE fingerprint=\''
        + fingerprint
        + '\' ORDER BY PUBLISHED DESC LIMIT 1;'
    )
    router = db.fetchone()
    db.close()
    conn.close()

    params_bw_read_hist = {
        "query": "read_bandwidth_history{fingerprint='" +
                 fingerprint + "'}",
        "start": "-30d",
        "end": "-5d",
        "step": "24h",
        "nocache": "1"
    }

    time_vector_bw_read = []
    values_vector_bw_read = []

    time_vector_bw_read, values_vector_bw_read = get_time_series(params_bw_read_hist)

    time_vector_bw_write = []
    values_vector_bw_write = []

    params_bw_write_hist = {
        "query": "write_bandwidth_history{fingerprint='" +
                 fingerprint + "'}",
        "start": "-30d",
        "end": "-5d",
        "step": "24h",
        "nocache": "1"
    }

    time_vector_bw_write, values_vector_bw_write = get_time_series(params_bw_write_hist)

    time_vector_exit = []
    values_vector_exit = []
    time_vector_middle = []
    values_vector_middle = []
    time_vector_guard = []
    values_vector_guard = []

    """
        If router is not a bridge compute the network fractions

        The first field in the status table is a boolean value which stores
        `True` when the node is a bridge.
    """
    if not router[0]:
        params_exit_fraction = {
            "query": "network_exit_fraction{fingerprint='" +
                     fingerprint + "'}",
            "start": "-30d",
            "end": "-5d",
            "step": "24h",
            "nocache": "1"
        }
        params_guard_fraction = {
            "query": "network_guard_fraction{fingerprint='" +
                     fingerprint + "'}",
            "start": "-30d",
            "end": "-5d",
            "step": "24h",
            "nocache": "1"
        }
        params_middle_fraction = {
            "query": "network_middle_fraction{fingerprint='" +
                     fingerprint + "'}",
            "start": "-30d",
            "end": "-5d",
            "step": "24h",
            "nocache": "1"
        }

        time_vector_exit, values_vector_exit = get_time_series(params_exit_fraction)
        time_vector_guard, values_vector_guard = get_time_series(params_guard_fraction)
        time_vector_middle, values_vector_middle = get_time_series(params_middle_fraction)

    context = {
        "title": fingerprint,
        "router": router,
        "read_bw": values_vector_bw_read,
        "read_bw_time": time_vector_bw_read,
        "write_bw": values_vector_bw_write,
        "write_bw_time": time_vector_bw_write,
        "time_exit": time_vector_exit,
        "values_exit": values_vector_exit,
        "time_guard": time_vector_guard,
        "values_guard": values_vector_guard,
        "time_middle": time_vector_middle,
        "values_middle": values_vector_middle,
    }
    return render_template('router/profile.html', **context)
