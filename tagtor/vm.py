"""
Time series database logic
"""
import json
import requests

from flask import (
    current_app
)
from datetime import datetime
from requests.auth import HTTPBasicAuth

TIME_FORMAT = '%Y-%m-%d'


def get_time_series(params):
    username = current_app.config['METRICS_DB_USER']
    password = current_app.config['METRICS_DB_PASSWORD']
    auth = HTTPBasicAuth(username, password)

    url = "https://metrics-db.torproject.org/prometheus/api/v1/query_range"

    response = requests.get(url, params, auth=auth)
    data = json.loads(response.text)

    time_vector = []
    values_vector = []

    for d in data["data"]["result"][0]["values"]:
        timestamp = datetime.utcfromtimestamp(d[0]).strftime(TIME_FORMAT)
        d1 = datetime.strptime(timestamp + "+0000", TIME_FORMAT + '%z')
        time_vector.append("{}".format(d1))
        values_vector.append(float(d[1]))

    return time_vector, values_vector
