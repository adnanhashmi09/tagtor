"""
Database logic
"""

import psycopg2


def get_db_connection():
    conn = psycopg2.connect(host='postgresdb',
                            database='metrics',
                            user='metrics',
                            password='password')
    return conn
