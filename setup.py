from setuptools import setup

setup(
    name='tagtor',
    version='0.0.1',
    author='Hiro',
    author_email='hiro@torproject.org',
    packages=['tagtor'],
)
